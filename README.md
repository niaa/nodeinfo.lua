nodeinfo.lua
============

Dead simple Lua CGI script for fetching information about Fediverse instances
using the nodeinfo API, targeting:

* Pleroma
* Diaspora
* Hubzilla
* PeerTube
* Misskey

Dependencies
------------

* Lua-cURL
* lua-cjson2
* lustache
* web_sanitize

Install using LuaRocks.
